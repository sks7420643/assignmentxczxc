package com.sarwar.assignment;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sarwar.assignment.dto.Discount;
import com.sarwar.assignment.dto.Order;
import com.sarwar.assignment.dto.Price;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SalesCalculator {

    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();

        List<Order> orders = readOrders(mapper);
        List<Price> prices = readPrices(mapper);
        List<Discount> discounts = readDiscounts(mapper);

        if (orders != null && prices != null && discounts != null) {
            Map<Integer, Double> priceMap = prices.stream().collect(Collectors.toMap(Price::getSku, Price::getPrice));
            Map<String, Double> discountMap = discounts.stream().collect(Collectors.toMap(Discount::getKey, Discount::getValue));

            double totalSalesBeforeDiscount = calculateTotalSalesBeforeDiscount(orders, priceMap);
            double totalSalesAfterDiscount = calculateTotalSalesAfterDiscount(orders, priceMap, discountMap);
            double totalLostRevenue = calculateTotalLostRevenue(orders, priceMap, discountMap);
            double averageDiscountPercentage = calculateAverageDiscountPerCustomer(orders, discountMap);

            System.out.println("Total sales before discount: $" + totalSalesBeforeDiscount);
            System.out.println("Total sales after discount: $" + totalSalesAfterDiscount);
            System.out.println("Total lost revenue: $" + totalLostRevenue);
            System.out.println("Average discount per customer: " + averageDiscountPercentage + "%");
        } else {
            System.out.println("Failed to read data from JSON files.");
        }
    }


    private static List<Order> readOrders(ObjectMapper mapper) {
        try (InputStream ordersStream = SalesCalculator.class.getResourceAsStream("/orders.json")) {
            return mapper.readValue(ordersStream, new TypeReference<List<Order>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Price> readPrices(ObjectMapper mapper) {
        try (InputStream pricesStream = SalesCalculator.class.getResourceAsStream("/products.json")) {
            return mapper.readValue(pricesStream, new TypeReference<List<Price>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Discount> readDiscounts(ObjectMapper mapper) {
        try (InputStream discountsStream = SalesCalculator.class.getResourceAsStream("/discounts.json")) {
            return mapper.readValue(discountsStream, new TypeReference<List<Discount>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static double calculateTotalSalesBeforeDiscount(List<Order> orders, Map<Integer, Double> priceMap) {
        return orders.stream()
                .flatMap(order -> order.getItems().stream())
                .mapToDouble(item -> item.getQuantity() * priceMap.getOrDefault(item.getSku(), 0.0))
                .sum();
    }

    public static double calculateTotalSalesAfterDiscount(List<Order> orders, Map<Integer, Double> priceMap, Map<String, Double> discountMap) {
        return orders.stream()
                .mapToDouble(order -> {
                    double orderTotal = order.getItems().stream()
                            .mapToDouble(item -> item.getQuantity() * priceMap.getOrDefault(item.getSku(), 0.0))
                            .sum();
                    double discountFactor = calculateDiscountFactor(order.getDiscount(), discountMap);
                    return orderTotal * discountFactor;
                })
                .sum();
    }

    public static double calculateTotalLostRevenue(List<Order> orders, Map<Integer, Double> priceMap, Map<String, Double> discountMap) {
        return orders.stream()
                .filter(order -> discountMap.containsKey(order.getDiscount()))
                .mapToDouble(order -> {
                    double orderTotal = order.getItems().stream()
                            .mapToDouble(item -> item.getQuantity() * priceMap.getOrDefault(item.getSku(), 0.0))
                            .sum();
                    double discountedTotal = orderTotal * calculateDiscountFactor(order.getDiscount(), discountMap);
                    return orderTotal - discountedTotal;
                })
                .sum();
    }

    public static double calculateAverageDiscountPerCustomer(List<Order> orders, Map<String, Double> discountMap) {
        return orders.stream()
                .filter(order -> discountMap.containsKey(order.getDiscount()))
                .mapToDouble(order -> discountMap.get(order.getDiscount()))
                .average()
                .orElse(0.0) * 100;
    }

    private static double calculateDiscountFactor(String discountCodes, Map<String, Double> discountMap) {
        if (discountCodes == null || discountCodes.isEmpty()) {
            return 1.0;
        }
        double factor = 1.0;
        for (String code : discountCodes.split(",")) {
            factor *= (1 - discountMap.getOrDefault(code.trim(), 0.0));
        }
        return 1 - factor;
    }
}
